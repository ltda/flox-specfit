# FLOX-SpecFit

This software function is intended to retrieve the Sun-Induced Chlorophyll 
Fluorescence (SIF) spectrum from radiance spectra acquired with the FLOX 
instrument (JB Hyperspectral Devices, Germany). In fact, it assumes that the
input data are FLOX data processed with the [FieldSpectroscopyDP R package](https://github.com/tommasojulitta/FieldSpectroscopyDP). 


## How to run the code
In order to use this code download the latest release from [here](https://gitlab.com/ltda/flox-specfit/-/releases) into a local directory.
Then follow these instruction for running it:
1. cd to FLOX-SpecFit code folder 
2. launch MATLAB
3. run 
```shell
>> FLOX_SpecFit_master('path_to_data_root_directory')
````


## Platforms
Windows 10, GNU/Linux (Debian 8+, Ubuntu 16.04+) and MacOS. Other versions of these OSs might also work but were not tested.


## Dependencies

**MATLAB Version: 9.5.0.1033004 (R2018b)** Update 3, with following toolboxes:

* Curve Fitting Toolbox
* Optimization Toolbox
* Parralel Computing Toolbox

Other versions of MATLAB might also work but were not tested.


## Copyrights
The copyrights for these 8 files, including the computer codes, remain with their authors, 
Drs. Sergio Cogliati, Marco Celesti and Roberto Colombo.


## Commercial and other users
Use of this code in commercial applications is strictly forbidden without the 
written approval of the authors. Even with such an authorization to use the code, 
you may not distribute or sell it to any other commercial or business 
partners under any circumstances.


## Academic users
You are authorized to use this code for your research and teaching, but you must acknowledge 
use of this routine explicitly and refer to the paper below in any publication or work for 
which you used these codes. You may distribute, free of charge, the unmodified version of these 
codes to colleagues involved in similar activities, provided you include all the in-line documentation. 
They, in turn, must agree with and abide by the same rules. You may not sell this code to anybody, 
and you may not distribute it to commercial interests under any circumstances.

## References
Cogliati, S., Celesti, M., Cesana, I., Miglietta, F., Genesio, L., Julitta, T., Schuettemeyer, D., Drusch, M., Rascher, U., Jurado, P., Colombo, R. (2019). A Spectral Fitting Algorithm to Retrieve the Fluorescence Spectrum from Canopy Radiance. Remote Sensing, 11(16), 1840. https://doi.org/10.3390/rs11161840
