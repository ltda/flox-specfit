function FLOX_SpecFit_master(data_path)   

% EXAMPLE
    % in the command window
    % FLOX_SFM_master('/home/user/FLOX_data/YYMMDD');
    % run

%%  Function to process FLOX data %%%%%%%%

%   Sergio Cogliati, Ph.D
%   Remote Sensing of Environmental Dynamics Lab.
%   University of Milano-Bicocca
%   Milano, Italy
%   email: sergio.cogliati@unimib.it
%
%   Marco Celesti, Ph.D
%   Remote Sensing of Environmental Dynamics Lab.
%   University of Milano-Bicocca
%   Milano, Italy
%   email: marco.celesti@unimib.it


%% --- STEPS



%% --- INITIALIZE LOG FILE

proc_time_all = datestr(now,'yyyymmdd_HH_MM_SS');
Logfile       = fullfile(data_path,strcat(proc_time_all,'_logfile.txt'));
diary(Logfile)

% suppress warnings
warning('off','all')

addpath(genpath(pwd));



%% --- ADJUST VARIABLES BEFORE RUN THE PROGRAM

% Adjust variables
wvlRet    = [670 780];

% Set options
opt_alg   = 'tr';    % optimization algorithm 
                       % 1) 'lm'= Levemberg-Marquard; 
                       % 2) 'tr'=trust region reflective
weights   = 1;       % weight options: 
                       % 1) w = 1;
                       % 2) w = (1/Lup)^2; 
                       % 3) w = (Lup)^2
stio      = 'off';   % options: 
                       % 1) 'off';
                       % 2) 'iter';
                       % 3) 'final'

% Saving options
save_each = 0;       % save_each option:
                       % 1 - saves all output files for each couple (Incoming/Reflected) of input radiance files together with summary output for all processed measurements
                       % 0 - saves only summary output for all processed measurements 
save_mat  = 1;       % save output files in a matlab variable in addition to .txt files



%% --- DEFINITION OF GLOBAL VARIABLES

global wvl_def %#ok<NUSED>
wvl_definition(4); 



%% --- INITIALIZE PARPOOL

profile      = 'local'; 
n_cores      = feature('numcores');        % automatically select the max number of available cores
p            = gcp('nocreate');            % If no pool, do not create new one.

if isempty(p)
    ppool    = parpool(profile,n_cores);   % ok<NOPTS>
else
    delete(p);
    ppool    = parpool(profile,n_cores);   % ok<NOPTS>
end

% Selects input files
L0_list      = rdir(fullfile(data_path,'**','Incoming*FLUO*.csv'));
L_list       = rdir(fullfile(data_path,'**','Reflected*FLUO*.csv'));

for n_tables = 1:numel(L0_list)
    
    tic
    
        fname_L0        = L0_list(n_tables).name;
        
        %
        fname_basename  = strsplit(fname_L0,filesep);
        fname_basename  = strsplit(fname_basename{end},'_'); 
        fname_basename  = strsplit(fname_basename{end},'.');
        fname_basename  = fname_basename{1};
        
        %   
        dir_L0          = strsplit(L0_list(n_tables).folder,filesep);
        dir_L0          = dir_L0{end};
    
        UTC_time        = readtable(fname_L0);
        UTC_time        = char(UTC_time.Properties.VariableNames(2:end).');
        UTC_time        = string(UTC_time(:,2:9));
    
        UTC_datetime    = datetime(strcat(repmat(dir_L0,size(UTC_time)),UTC_time),'InputFormat','yyMMddHH_mm_ss');
        DOYdayfrac      = date2doy(datenum(UTC_datetime));
    
        wvl_QEPRO       = dlmread(fname_L0,';',1,0);
        wvl_QEPRO       = wvl_QEPRO(:,1);
        L0_table        = dlmread(fname_L0,';',1,1);
    
        fname_L         = L_list(n_tables).name;
        L_table         = dlmread(fname_L,';',1,1);
    
        n_files         = size(L0_table,2);
    
        fprintf('Processing file %i of %i\n%s\n',n_tables,numel(L0_list),fname_L0)
        
        %% -- Wavelength definition
        wvl             = wvl_QEPRO;
        [~, lb]         = min(abs(wvl-wvlRet(1))); 
        [~, ub]         = min(abs(wvl-wvlRet(2)));
        
        %% -- Some basic filtering
        L0_filter       = L0_table(500,:)>=0.01;
    
    
        %% -- Spectral subset of input spectra to min_wvl - max_wvl range and convert to mW 
        wvl             = wvl(lb:ub);
        Lin             = L0_table(lb:ub,:)*1e3;
        Lup             = L_table(lb:ub,:)*1e3;
        
        % define output wvl for SpecFit
        owvl            = wvl;
        [~,iowvl_760]   = min(abs(owvl-760));
        [~,iowvl_687]   = min(abs(owvl-687));
    
        % creates the output header
        out_hdr         = {'DOYdayfrac' 'UTC_datetime' 'filenum'                                                                       ...
                           'f_SpecFit_A' 'r_SpecFit_A' 'f_SpecFit_B' 'r_SpecFit_B' 'resnorm_SpecFit' 'exitflag_SpecFit' 'n_it_SpecFit' ...
                           'f_max_A' 'f_max_A_wvl' 'f_max_B' 'f_max_B_wvl' 'f_int'                                                     ...
                          };

        % save spectra from SpecFit
        out_mat         = nan(n_files,size(out_hdr,2)-8);
        outF_SpecFit    = nan(numel(owvl),n_files);
        outR_SpecFit    = nan(numel(owvl),n_files);
        
        %% -- weighting scheme
        switch weights  
            case 1
                w_F     = ones(size(Lup,1),1);  
                w_F(:)  = 1.0;
                
            case 2
                w_F     = (1./Lup);
                
            case 3
                w_F     = (Lup.^2);
                
            otherwise        
        end
    
        
        parfor i = 1:n_files
            
            % -- process only filtered data
            if L0_filter(i) == 1           
                
                % Processing                                                                           
                [x_F,f_wvl_F,r_wvl_F,resnorm_F,exitflag_F,output_F] = FLOX_SpecFit_6C...
                                                                        (wvl,Lin(:,i),Lup(:,i),[1,1],w_F,opt_alg,stio,wvl);
                %
                out_mat(i,:) = [ ...
                    f_wvl_F(iowvl_760) r_wvl_F(iowvl_760) f_wvl_F(iowvl_687) r_wvl_F(iowvl_687) resnorm_F exitflag_F output_F.iterations ...
                    ];
            
                % spectral output from SpecFit
                outF_SpecFit(:,i) = f_wvl_F;
                outR_SpecFit(:,i) = r_wvl_F;
                
            end
            
        end
    
       %% --
       out_arr = [string(DOYdayfrac) string(UTC_datetime) string((1:n_files)') string(out_mat)];
     
       if  n_tables == 1
            
            out_arr_all      = out_arr;
            outF_SpecFit_all = outF_SpecFit;
            outR_SpecFit_all = outR_SpecFit;
            
       else
            
            out_arr_all      = [out_arr_all; out_arr]; %#ok<*AGROW>
            outF_SpecFit_all = [outF_SpecFit_all,outF_SpecFit];
            outR_SpecFit_all = [outR_SpecFit_all,outR_SpecFit];
            
       end
    
      %% -- compute FLEX metrics
      [SIF_R_max,SIF_R_wl,~,SIF_FR_max,SIF_FR_wl,~,SIFint] = sif_parms(owvl,outF_SpecFit); 
    
      %% -- write output file -> only if save_each option is enabled and there is more than one table 
      if ~~save_each && numel(L0_list) > 1
            
            % write index
            out_arr                     = [out_arr SIF_FR_max' SIF_FR_wl SIF_R_max' SIF_R_wl SIFint'];
            out_arr(ismissing(out_arr)) = "NaN";
            out_table                   = array2table(out_arr);
            out_varnames                = matlab.lang.makeValidName(out_hdr);
            out_varnames                = matlab.lang.makeUniqueStrings(out_varnames);
            
            out_table.Properties.VariableNames = out_varnames;
            out_fname                   = strcat(L0_list(n_tables).folder,filesep,proc_time_all,'_CF_Index_matlab_NBFretVPSPLINE_SpecFit_6C_',fname_basename,'.txt');
            writetable(out_table,out_fname,'Delimiter',';');
            
            % write spectra
            out_varnames_SpecFit        = ['wvl' cellstr(out_table.UTC_datetime).'];
                % F
                out_arr_F_SpecFit       = [owvl outF_SpecFit];
                out_fname_SpecFit       = strcat(L0_list(n_tables).folder,filesep,proc_time_all,'_CF_Index_matlab_FLOX_SpecFit_6C_F_',fname_basename,'.txt');
                csvwrite_with_headers(out_fname_SpecFit,out_arr_F_SpecFit,out_varnames_SpecFit);
                % R
                out_arr_R_SpecFit       = [owvl outR_SpecFit];
                out_fname_SpecFit       = strcat(L0_list(n_tables).folder,filesep,proc_time_all,'_CF_Index_matlab_FLOX_SpecFit_6C_R_',fname_basename,'.txt');
                csvwrite_with_headers(out_fname_SpecFit,out_arr_R_SpecFit,out_varnames_SpecFit);
        
            if ~~save_mat
                out_mat_filename        = fullfile(strcat(L0_list(n_tables).folder,filesep,proc_time_all,'_matlab_FLOX_NBFretVPSPLINE_SpecFit_6C_out_',fname_basename,'.mat'));
                save(out_mat_filename,'out_table','out_arr_F_SpecFit','out_arr_R_SpecFit','out_varnames_SpecFit','L0_table','L_table','owvl')
            end
            
       end
        
    toc
    
end



%% --- Compute FLEX metrics

[SIF_R_max,SIF_R_wl,~,SIF_FR_max,SIF_FR_wl,~,SIFint] = sif_parms(owvl,outF_SpecFit_all); % move this above


%% --- Write output file with all data

% write index
out_arr                            = [out_arr_all SIF_FR_max' SIF_FR_wl SIF_R_max' SIF_R_wl SIFint'];
out_arr(ismissing(out_arr))        = "NaN";
out_table                          = array2table(out_arr);
out_varnames                       = matlab.lang.makeValidName(out_hdr);
out_varnames                       = matlab.lang.makeUniqueStrings(out_varnames);
out_table.Properties.VariableNames = out_varnames;
out_fname                          = fullfile(data_path,strcat(proc_time_all,'_CF_Index_matlab_NBFretVPSPLINE_SpecFit_6C_allmeas.txt'));

writetable(out_table,out_fname,'Delimiter',';');

% write spectra
out_varnames_SpecFit               = ['wvl' cellstr(out_table.UTC_datetime).'];

% F
out_arr_F_SpecFit                  = [owvl outF_SpecFit_all];
out_fname_SpecFit                  = fullfile(data_path,strcat(proc_time_all,'_CF_Index_matlab_FLOX_SpecFit_6C_F_allmeas.txt'));

csvwrite_with_headers(out_fname_SpecFit,out_arr_F_SpecFit,out_varnames_SpecFit);

% R
out_arr_R_SpecFit                  = [owvl outR_SpecFit_all];
out_fname_SpecFit                  = fullfile(data_path,strcat(proc_time_all,'_CF_Index_matlab_FLOX_SpecFit_6C_R_allmeas.txt'));

csvwrite_with_headers(out_fname_SpecFit,out_arr_R_SpecFit,out_varnames_SpecFit);


if ~~save_mat
    out_mat_filename = fullfile(data_path,strcat(proc_time_all,'_matlab_FLOX_NBFretVPSPLINE_SpecFit_6C_allmeas_out.mat'));
    save(out_mat_filename,'out_table','out_arr_F_SpecFit','out_arr_R_SpecFit','out_varnames_SpecFit','owvl')
end


%% --- CLOSE PARPOOL

delete(ppool);

%% --- STOP LOGGING

diary off;
disp('Finished!');

